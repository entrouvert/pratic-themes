VERSION=`git describe | sed 's/^v//; s/-/./g' `
NAME="pratic-themes"

prefix = /usr

all: build

clean:
	rm -rf build sdist

build: clean
	mkdir -p build/$(NAME)-$(VERSION)
	for i in *; do \
		if [ "$$i" != "build" ]; then \
			cp -R "$$i" build/$(NAME)-$(VERSION); \
		fi; \
	done

install:
	mkdir -p $(DESTDIR)$(prefix)/share/authentic2/pratic
	mkdir -p $(DESTDIR)$(prefix)/share/wcs/themes/pratic
	cp -r authentic2/* $(DESTDIR)$(prefix)/share/authentic2/pratic
	cp -ar wcs/* $(DESTDIR)$(prefix)/share/wcs/themes/

dist-bzip2: build
	mkdir sdist
	cd build && tar cfj ../sdist/$(NAME)-$(VERSION).tar.bz2 .

version:
	@(echo $(VERSION))

name:
	@(echo $(NAME))

fullname:
	@(echo $(NAME)-$(VERSION))

